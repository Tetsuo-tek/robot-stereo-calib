#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.abstractflow import FlowChannel
from PySketch.flowproto import FlowChanID, Flow_T, Variant_T
from PySketch.flowsat import RobotModule
from PySketch.elapsedtime import ElapsedTime

import os
import json
import cv2
import numpy as np

###############################################################################
# GLOBAL

sat = None

srcName = None
src = None
chrono = ElapsedTime()

requiredToSavePair = False
calibSet0 = []
calibSet1 = []

calibrated = False
colors = [(0,0,255), (0,255,0), (255,0,0)]

#project 3D points to each camera view manually. This can also be done using cv.projectPoints()
#Note that this uses homogenous coordinate formulation
pixelPoints0 = []
pixelPoints1 = []

###############################################################################
#SKETCH

def setup() -> bool:
    global sat
    global srcName

    sat = RobotModule() 
    
    if not sat.connect():
        return False
    
    sat.setNewChanCallBack(onChannelAdded)
    sat.setGrabDataCallBack(onDataGrabbed)
    
    srcName = "guest.STEREO.JPeg"

    #frame = Nonenp.zeros((h, w, 3), dtype=np.uint8)

    print("Press:")
    print(" - 's' to save stereo-frame pair")
    print(" - 'c' execute calibration, for left, right and stereo")
    print(" - 'q' to quit")

    return True

def loop() -> bool:
    global sat
    global calibrated

    sat.tick()

    k = cv2.waitKey(1) & 0xFF
    
    if not sat.isConnected() and calibrated:
        if not sat.connect():
            return False
            
    return onKeyPressed(k)

###############################################################################
#CALLBACKs

def onChannelAdded(ch: FlowChannel):
    global sat
    global src
    global srcName

    if ch.name == srcName:
        src = ch
        sat.subscribeChannel(src.chanID)

def onDataGrabbed(chanID: FlowChanID, data: bytearray):
    global src
    global sat
    global requiredToSavePair
    global calibSet0
    global calibSet1
    global colors
    global calibrated
    global chrono

    if chanID == src.chanID:

        if chrono.stop() <= 0.5:
            return

        chrono.start()

        np_array = np.frombuffer(data, np.uint8)
        frame = cv2.imdecode(np_array, cv2.IMREAD_COLOR)

        if frame is None or frame.size == 0:
            return

        h, w, _ = frame.shape
        w //= 2
        
        f0 = frame[:, :w, :]
        f1 = frame[:, w:, :]

        if not calibrated:
            if requiredToSavePair:
                calibSet0.append(f0)
                calibSet1.append(f1)

                requiredToSavePair = False
                
                frame = np.ones((frame.shape[0], frame.shape[1], 3), dtype=np.uint8) * 255
                print(f"Added new pair-frame to stereo calib-set: {len(calibSet0)} elements")

            else:
                calibPreview(f0, f1)

        else:
            #draw projections to camera0
            origin = tuple(pixelPoints0[0].astype(np.int32))
            for col, _p in zip(colors, pixelPoints0[1:]):
                _p = tuple(_p.astype(np.int32))
                cv2.line(f0, origin, _p, col, 2)

            #draw projections to camera1
            origin = tuple(pixelPoints1[0].astype(np.int32))
            for col, _p in zip(colors, pixelPoints1[1:]):
                _p = tuple(_p.astype(np.int32))
                cv2.line(f1, origin, _p, col, 2)

            frame = cv2.hconcat([f0, f1])

        cv2.imshow("Frame", frame)
            

###############################################################################
# PREVIEW

def calibPreview(f0, f1):
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.0001)
    
    rows = 5
    columns = 7
    world_scaling = 2.8

    objp = np.zeros((rows*columns,3), np.float32)
    objp[:,:2] = np.mgrid[0:rows,0:columns].T.reshape(-1,2)
    objp = world_scaling * objp

    width = f0.shape[1]
    height = f0.shape[0]

    imgpoints_left = []
    imgpoints_right = []
    objpoints = []

    gray1 = cv2.cvtColor(f0, cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(f1, cv2.COLOR_BGR2GRAY)

    c_ret1, corners1 = cv2.findChessboardCorners(gray1, (rows, columns), None)
    c_ret2, corners2 = cv2.findChessboardCorners(gray2, (rows, columns), None)
 
    if c_ret1 == True and c_ret2 == True:
        corners1 = cv2.cornerSubPix(gray1, corners1, (11, 11), (-1, -1), criteria)
        corners2 = cv2.cornerSubPix(gray2, corners2, (11, 11), (-1, -1), criteria)

        cv2.drawChessboardCorners(f0, (rows,columns), corners1, c_ret1)
        cv2.drawChessboardCorners(f1, (rows,columns), corners2, c_ret2)

    frame = cv2.hconcat([f0, f1])
    cv2.imshow("Frame", frame)

###############################################################################
# CONTROL

def onKeyPressed(key):
    global sat
    global requiredToSavePair

    if key == 0xFF:
        return True

    if key == ord('q'):
        if sat.isConnected():
            sat.disconnect()

        cv2.destroyAllWindows()
        return False

    elif key == ord('s'):
        requiredToSavePair = True

    elif key == ord('c'):
        executeCalibration()
        return False
        #return executeCalibration()
    
    return True

###############################################################################
# CALIBRATION    

def executeCalibration():
    global sat
    global calibSet0
    global calibSet1
    global calibrated

    sat.disconnect()
    cv2.destroyAllWindows()

    print("PASS 1 - Executing LEFT calibration ..")
    ok, mtx0, dist0 = monoCalibrate(calibSet0)

    if not ok:
        return False

    print("PASS 2 - Executing RIGHT calibration ..")
    ok, mtx1, dist1 = monoCalibrate(calibSet1) 

    if not ok:
        return False

    cv2.destroyAllWindows()

    saveCameraIntrinsics(mtx0, dist0, '0')
    saveCameraIntrinsics(mtx1, dist1, '1')

    print("PASS 3 - Executing STEREO calibration ..")
    ok, R, T = stereoCalibrate(mtx0, dist0, mtx1, dist1)

    if ok:
        R_L = np.eye(3, dtype=np.float32)
        T_L = np.array([0., 0., 0.]).reshape((3, 1))

        saveExtrinsicCalibrationParameters(R_L, T_L, '0')
        saveExtrinsicCalibrationParameters(R, T, '1')

        #to avoid confusion, camera1 R and T are labeled R1 and T1
        R_R = R
        T_R = T 

        l_data = [mtx0, dist0, R_L, T_L]
        r_data = [mtx1, dist1, R_R, T_R]

        calibrated = True
        #checkCalibration('left', l_data, 'right', r_data, _zshift = 60.)

        print("Stereo calibration SUCCESS")
        cv2.destroyAllWindows()
        return True

    else:
        print("Stereo calibration FAILED")

    cv2.destroyAllWindows()
    return False

def monoCalibrate(frames):
    if not frames:
        print("Frames list is EMPTY")
        return False, 0, 0

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
 
    rows = 5 #number of checkerboard rows.
    columns = 7 #number of checkerboard columns.
    world_scaling = 2.8 #change this to the real world square size. Or not.
 
    #coordinates of squares in the checkerboard world space
    objp = np.zeros((rows*columns,3), np.float32)
    objp[:,:2] = np.mgrid[0:rows,0:columns].T.reshape(-1,2)
    objp = world_scaling * objp
 
    #frame dimensions. Frames should be the same size.
    width = frames[0].shape[1]
    height = frames[0].shape[0]
 
    #Pixel coordinates of checkerboards
    imgpoints = [] # 2d points in image plane.
 
    #coordinates of the checkerboard in checkerboard world space.
    objpoints = [] # 3d point in real world space
 
    print(f"Analizing {len(frames)} frames ..")

    for frame in frames:
        f = frame.copy()
        gray = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
 
        #find the checkerboard
        ret, corners = cv2.findChessboardCorners(gray, (rows, columns), None)

        if ret == True:
 
            #Convolution size used to improve corner detection. Don't make this too large.
            conv_size = (11, 11)
 
            #opencv can attempt to improve the checkerboard coordinates
            corners = cv2.cornerSubPix(gray, corners, conv_size, (-1, -1), criteria)
            cv2.drawChessboardCorners(f, (rows,columns), corners, ret)
            cv2.imshow('Mono calibrate', f)
 
            print("Press some key to continue")
            k = cv2.waitKey(-1)

            objpoints.append(objp)
            imgpoints.append(corners)

        else:
            cv2.imshow('Mono calibrate', frame)

            print("No chessboard cornes found in this frame; press some key to continue")
            k = cv2.waitKey(-1)

    if not objpoints or not imgpoints:
        print("Camera is NOT calibrated; chessboard NOT found on frames list")
        return False, 0, 0

    rmse, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, (width, height), None, None)

    print('rmse:', rmse)
    print('camera matrix:\n', mtx)
    print('distortion coeffs:', dist)
    print('Rs:\n', rvecs)
    print('Ts:\n', tvecs)
 
    return True, mtx, dist

def stereoCalibrate(mtx0, dist0, mtx1, dist1):
    global calibSet0
    global calibSet1

    #change this if stereo calibration not good.
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.0001)
    
    rows = 5 #number of checkerboard rows.
    columns = 7 #number of checkerboard columns.
    world_scaling = 2.8 #change this to the real world square size. Or not.
    
    #coordinates of squares in the checkerboard world space
    objp = np.zeros((rows*columns,3), np.float32)
    objp[:,:2] = np.mgrid[0:rows,0:columns].T.reshape(-1,2)
    objp = world_scaling * objp
    
    #frame dimensions. Frames should be the same size.
    width = calibSet0[0].shape[1]
    height = calibSet0[0].shape[0]
    
    #Pixel coordinates of checkerboards
    imgpoints_left = [] # 2d points in image plane.
    imgpoints_right = []
    
    #coordinates of the checkerboard in checkerboard world space.
    objpoints = [] # 3d point in real world space
    
    for f0, f1 in zip(calibSet0, calibSet1):
        gray1 = cv2.cvtColor(f0, cv2.COLOR_BGR2GRAY)
        gray2 = cv2.cvtColor(f1, cv2.COLOR_BGR2GRAY)

        c_ret1, corners1 = cv2.findChessboardCorners(gray1, (rows, columns), None)
        c_ret2, corners2 = cv2.findChessboardCorners(gray2, (rows, columns), None)
 
        if c_ret1 == True and c_ret2 == True:
            corners1 = cv2.cornerSubPix(gray1, corners1, (11, 11), (-1, -1), criteria)
            cv2.drawChessboardCorners(f0, (rows,columns), corners1, c_ret1)
            
            corners2 = cv2.cornerSubPix(gray2, corners2, (11, 11), (-1, -1), criteria)
            cv2.drawChessboardCorners(f1, (rows,columns), corners2, c_ret2)

            frame = cv2.hconcat([f0, f1])
            cv2.imshow('Stereo calibrate', frame)
 
            print("Press some key to continue")
            cv2.waitKey(-1)
 
            objpoints.append(objp)
            imgpoints_left.append(corners1)
            imgpoints_right.append(corners2)

        else:
            print("No chessboard cornes found in this frame; press some key to continue")
            k = cv2.waitKey(-1)
 
    if not objpoints or not imgpoints_left or not imgpoints_right:
        print("Camera is NOT calibrated; chessboard NOT found on frames list")
        return False, 0, 0

    stereocalibration_flags = cv2.CALIB_FIX_INTRINSIC
    ret, CM1, dist0, CM2, dist1, R, T, E, F = cv2.stereoCalibrate(objpoints,
                                                                  imgpoints_left,
                                                                  imgpoints_right,
                                                                  mtx0, dist0,
                                                                  mtx1, dist1,
                                                                  (width, height),
                                                                  criteria = criteria,
                                                                  flags = stereocalibration_flags)
  
    print(ret)
    return True, R, T

def saveCameraIntrinsics(mtx, dist, cameraID):

    if not os.path.exists('calib-data'):
        os.mkdir('calib-data')

    out_filename = os.path.join('calib-data', cameraID + '-intrinsics.dat')
    outf = open(out_filename, 'w')

    matrix = []
    distortion = []

    for l in mtx:
        for v in l:
            matrix.append(v)

    for v in dist[0]:
        distortion.append(v)
            
    data = {"matrix" : matrix, "distortion" : distortion}
    outf.write(json.dumps(data, indent=2))

    outf.close()


def saveExtrinsicCalibrationParameters(R, T, cameraID):

    if not os.path.exists('calib-data'):
        os.mkdir('calib-data')

    out_filename = os.path.join('calib-data', cameraID + '-extrinsics.dat')
    outf = open(out_filename, 'w')

    rotation = []
    translation = []

    for l in R:
        for v in l:
            rotation.append(float(v))

    for l in T:
        for v in l:
            translation.append(float(v))

    data = {"rotation" : rotation, "translation" : translation}
    outf.write(json.dumps(data, indent=2))

    outf.close()


###############################################################################
'''
#Converts Rotation matrix R and Translation vector T into a homogeneous representation matrix
def _make_homogeneous_rep_matrix(R, t):
    P = np.zeros((4,4))
    P[:3,:3] = R
    P[:3, 3] = t.reshape(3)
    P[3,3] = 1
 
    return P

# Turn camera calibration data into projection matrix
def get_projection_matrix(cmtx, R, T):
    P = cmtx @ _make_homogeneous_rep_matrix(R, T)[:3,:]
    return P

def checkCalibration(camera0_name, camera0_data, camera1_name, camera1_data, _zshift = 50.):
    global pixelPoints0
    global pixelPoints1

    print("Checking calibration")

    cmtx0 = np.array(camera0_data[0])
    dist0 = np.array(camera0_data[1])
    R0 = np.array(camera0_data[2])
    T0 = np.array(camera0_data[3])

    cmtx1 = np.array(camera1_data[0])
    dist0 = np.array(camera1_data[1])
    R1 = np.array(camera1_data[2])
    T1 = np.array(camera1_data[3])

    P0 = get_projection_matrix(cmtx0, R0, T0)
    P1 = get_projection_matrix(cmtx1, R1, T1)

    #define coordinate axes in 3D space. These are just the usual coorindate vectors
    coordinate_points = np.array([[0.,0.,0.],
                                  [1.,0.,0.],
                                  [0.,1.,0.],
                                  [0.,0.,1.]])

    z_shift = np.array([0.,0.,_zshift]).reshape((1, 3))
    #increase the size of the coorindate axes and shift in the z direction
    draw_axes_points = 5 * coordinate_points + z_shift

    for _p in draw_axes_points:
        X = np.array([_p[0], _p[1], _p[2], 1.])
        
        #project to camera0
        uv = P0 @ X
        uv = np.array([uv[0], uv[1]])/uv[2]
        pixelPoints0.append(uv)

        #project to camera1
        uv = P1 @ X
        uv = np.array([uv[0], uv[1]])/uv[2]
        pixelPoints1.append(uv)

    #these contain the pixel coorindates in each camera view as: (pxl_x, pxl_y)
    pixelPoints0 = np.array(pixelPoints0)
    pixelPoints1 = np.array(pixelPoints1)
'''
